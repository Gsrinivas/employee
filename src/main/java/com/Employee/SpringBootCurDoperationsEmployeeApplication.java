package com.Employee;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootCurDoperationsEmployeeApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootCurDoperationsEmployeeApplication.class, args);
	}

}
